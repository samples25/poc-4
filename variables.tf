/*
variable "var_label" {
  description = ""
}
*/

variable "ingress-workstation" {
  description = "local workstation external IP"
  type        = list
  default =  []
}

variable "k8s-cluster-name" {
  default = "terraform-eks-poc4"
  type    = "string"
}

variable "kafka-topics" {
  type = "list"
  default = [
    {
      "topic" = "test"
      "replication-factor" = 1
      "partitions" = 4
    }
  ]
}

variable "config_map_mapusers" {

}