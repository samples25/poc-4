//Docker repo
output "ecr_url" {
  value = "${module.aws-resources.ecr_url}"
}

//Kafka

output "zookeeper_connect_string" {
  value = "${module.aws-resources.zookeeper_connect_string}"
}

output "bootstrap_brokers" {
  description = "Plaintext connection host:port pairs"
  value       = "${module.aws-resources.bootstrap_brokers}"

}

output "bootstrap_brokers_tls" {
  description = "TLS connection host:port pairs"
  value = "${module.aws-resources.bootstrap_brokers_tls}"
}


// K8s

output "endpoint" {
  value = "${module.aws-resources.endpoint}"
}

output "kubeconfig-certificate-authority-data" {
  value = "${module.aws-resources.kubeconfig-certificate-authority-data}"
  sensitive   = true
}


//kubeconfig for kubectl
output "kubeconfig" {
  value = "${module.aws-resources.kubeconfig}"
}


//eks configmap output
output "config_map_aws_auth" {
  value = "${module.aws-resources.config_map_aws_auth}"
}

//es
output "kibana_endpoint" {
  value = "${module.aws-resources.kibana_endpoint}"
}

output "es_endpoint" {
  value = "${module.aws-resources.es_endpoint}"
}

// grafana
output "grafana-point" {
  value = "${module.helm.grafana-point}"
}

output "grafana-admin-pass" {
  value = "${module.helm.grafana-admin-passwd}"
}