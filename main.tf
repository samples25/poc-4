terraform {
  backend "s3" {
    bucket = "poc4-infra"
    key    = "terraform/terraform.tfstate"
    region = "ap-southeast-1"
  }
}

provider "aws" {
  version = "2.19.0"
  region = "ap-southeast-1"
}

provider "aws" {
  version = "2.19.0"
  alias = "aws-singapour-provider"
  region = "ap-southeast-1"
}

provider "kubernetes" {
  version = "1.8.0"
  alias = "k8s-provider"
}

provider "helm" {
  version = "0.10.0"
  alias = "helm-provider"
  service_account = "tiller"
  automount_service_account_token = true
  kubernetes {
    load_config_file = false
  }
}


module "aws-resources" {
  source    = "./modules/aws-resources"
  providers = {
    aws = "aws.aws-singapour-provider"
  }
  ingress-workstation = "${var.ingress-workstation}"
}

module "k8s" {
  source = "./modules/kubernetes"
  providers = {
    kubernetes = "kubernetes.k8s-provider"
  }
  k8s-cluster-name = "${var.k8s-cluster-name}"
  current_aws_region = "ap-southeast-1"
  config_map_maproles = "${module.aws-resources.config_map_maproles}"
  kubeconfig = "${module.aws-resources.kubeconfig}"
  es_endpoint = "${module.aws-resources.es_endpoint}"
  kafka_endpoint = "${module.aws-resources.bootstrap_brokers_tls}"
  zookeeper_nodes = "${module.aws-resources.zookeeper_connect_string}"
}

module "helm" {
  source = "./modules/helm"
  providers = {
    helm = "helm.helm-provider"
    kubernetes = "kubernetes.k8s-provider"
  }
  tiller-id = "${module.k8s.tiller}"
}

/*
module "lambda" {
  source    = "./modules/lambda"
  depends_on=["module.aws-resources"]
}
*/

module "kafka" {
  source = "./modules/kafka"
  providers = {
    kubernetes = "kubernetes.k8s-provider"
  }
  k8s-cluster-name = "${var.k8s-cluster-name}"
  current_aws_region = "ap-southeast-1"
  config_map_maproles = "${module.aws-resources.config_map_maproles}"
  kubeconfig = "${module.aws-resources.kubeconfig}"
  kafka-topics = "${var.kafka-topics}"
  zookeeper_connect_string = "${module.aws-resources.zookeeper_connect_string}"
  bootstrap_brokers_tls = "${module.aws-resources.bootstrap_brokers_tls}"
}

provider "grafana" {
  version = "1.5.0"
  alias = "grafana-provider"
  auth = "admin:${module.helm.grafana-admin-passwd}"
  url = "http://${module.helm.grafana-point.0.hostname}"
}

module "grafana" {
  source = "./modules/grafana"
  providers = {
    grafana = "grafana.grafana-provider"
  }
}