#!/bin/bash

action=$1



cd ~/IaC-POC4
git stash

/usr/local/bin/terraform init -input=false

if [[ "$action" == "apply" ]]
then
  git pull origin
  echo "Starting resources creation"
  /usr/local/bin/terraform apply -input=false -auto-approve -target=module.aws-resources && \
  sleep 60 && \
  /usr/local/bin/terraform apply -input=false -auto-approve -target=module.k8s && \
  /usr/local/bin/terraform apply -input=false -auto-approve -target=module.helm && \
  sleep 60 && \
  /usr/local/bin/terraform apply -input=false -auto-approve -target=module.grafana && \
  #/usr/local/bin/terraform apply -input=false -auto-approve -target=module.kafka.null_resource.topics-creation-triggers && \
  #/usr/local/bin/terraform apply -input=false -auto-approve
  cd ~/pwc-k8s
  git pull
  kubectl apply -f ~/pwc-k8s/05-cluster-operator-all-namespaces
  kubectl apply -f ~/pwc-k8s/ -n poc4
  echo "Creation complete!"
elif [[ "$action" == "destroy" ]]
then
  echo "Starting resources destroying"
  kubectl delete svc --all -n poc4 &&\
  kubectl delete pv --all &&\
  /usr/local/bin/terraform destroy -input=false -auto-approve -target=module.helm && \
  /usr/local/bin/terraform destroy -input=false -auto-approve -target=module.k8s && \
  /usr/local/bin/terraform state rm module.helm &&\
  /usr/local/bin/terraform state rm module.grafana &&\
  /usr/local/bin/terraform state rm module.kafka &&\
  /usr/local/bin/terraform destroy -input=false -auto-approve -target=module.aws-resources
  echo "Destroy complete!"
fi
