output "grafana-point" {
  value = "${data.kubernetes_service.grafana.load_balancer_ingress}"
}

output "grafana-admin-passwd" {
  value = "${random_string.grafana_password.result}"
  sensitive = true
}