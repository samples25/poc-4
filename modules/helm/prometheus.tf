resource "helm_release" "prometheus" {
  namespace = "prometheus"
  chart = "stable/prometheus"
  name = "prometheus"
  set {
    name = "alertmanager.persistentVolume.storageClass"
    value = "gp2"
  }
  set {
    name = "server.persistentVolume.storageClass"
    value = "gp2"
  }
  depends_on = ["var.tiller-id"]
}

resource "random_string" "grafana_password" {
  length = 12
  special = false
}

resource "helm_release" "grafana" {
  chart = "stable/grafana"
  name = "grafana"
  namespace = "grafana"
  set {
    name = "persistence.storageClassName"
    value = "gp2"
  }
  set {
    name = "adminPassword"
    value = "${random_string.grafana_password.result}"
  }
  set {
    name = "datasources.\"datasources\\.yaml\".apiVersion"
    value = 1
  }
  set {
    name = "datasources.\"datasources\\.yaml\".datasources[0].name"
    value = "Prometheus"
  }
  set {
    name = "datasources.\"datasources\\.yaml\".datasources[0].type"
    value = "prometheus"
  }
  set {
    name = "datasources.\"datasources\\.yaml\".datasources[0].url"
    value = "http://prometheus-server.prometheus.svc.cluster.local"
  }
  set {
    name = "datasources.\"datasources\\.yaml\".datasources[0].access"
    value = "proxy"
  }
  set {
    name = "datasources.\"datasources\\.yaml\".datasources[0].isDefault"
    value = true
  }
  set {
    name = "service.type"
    value = "LoadBalancer"
  }

  depends_on = ["var.tiller-id"]
}

data "kubernetes_service" "grafana" {
  metadata {
    name = "grafana"
    namespace = "grafana"
  }
  depends_on = ["helm_release.grafana"]
}
