variable "k8s-cluster-name" {

}

variable "current_aws_region" {

}

variable "config_map_maproles" {}

variable "kubeconfig" {}

variable "kafka-topics" {
  type = "list"
  default = [
    {
      "topic" = "my-topic"
      "replication-factor" = 3
      "partitions" = 1
    }
  ]
}

variable "zookeeper_connect_string" {
}

variable "bootstrap_brokers_tls" {
}