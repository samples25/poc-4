/*resource "kubernetes_namespace" "kafka-admin" {
  metadata {
    name = "kafka-admin"
  }
}

resource "kubernetes_pod" "kafka-admin" {
  "metadata" {
    generate_name = "kafka-admin"
    namespace = "kafka-admin"
  }
  "spec" {
    init_container {
      name = ""
      command = []

    }
    restart_policy = "Never"
  }
}*/

resource "null_resource" "topics-update-triggers" {
  count = "${length("${var.kafka-topics}")}"
  triggers = {
    //zookeeper = list("${var.zookeeper_connect_string}")
    //bootstrap-brokers = list("${var.bootstrap_brokers_tls}")
    replication-factor = lookup("${var.kafka-topics[count.index]}","replication-factor")
    partitions = lookup("${var.kafka-topics[count.index]}","partitions")
  }

  provisioner "local-exec" {
    command = <<EOF
    kubectl run -it  kafka-admin-update-${count.index} \
                --image=bitnami/kafka \
                --rm=true --restart=Never \
                -- \
                /opt/bitnami/kafka/bin/kafka-topics.sh --alter \
                --zookeeper ${var.zookeeper_connect_string}  \
                --replication-factor ${var.kafka-topics[count.index].replication-factor} \
                --partitions ${var.kafka-topics[count.index].partitions} \
                --topic ${var.kafka-topics[count.index].topic}
    EOF
  }
}


resource "null_resource" "topics-creation-triggers" {
  count = "${length("${var.kafka-topics}")}"
  triggers = {
    //zookeeper = list("${var.zookeeper_connect_string}")
    //bootstrap-brokers = list("${var.bootstrap_brokers_tls}")
    topics = lookup("${var.kafka-topics[count.index]}","topic")
  }

  provisioner "local-exec" {
    command = <<EOF
    kubectl run -it  kafka-admin-create-${count.index} \
                --image=bitnami/kafka \
                --rm=true --restart=Never \
                -- \
                /opt/bitnami/kafka/bin/kafka-topics.sh --create \
                --zookeeper ${var.zookeeper_connect_string}  \
                --replication-factor ${var.kafka-topics[count.index].replication-factor} \
                --partitions ${var.kafka-topics[count.index].partitions} \
                --topic ${var.kafka-topics[count.index].topic}
    EOF
  }
}

