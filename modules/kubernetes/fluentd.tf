resource "kubernetes_service_account" "fluentd" {
  metadata {
    name = "fluentd"
    namespace = "kube-system"
  }
  automount_service_account_token = true
}

resource "kubernetes_cluster_role" "fluentd" {
  metadata {
    name = "fluentd"
  }
  rule {
    api_groups = [
      ""]
    resources = [
      "namespaces",
      "pods"]
    verbs = [
      "get",
      "list",
      "watch"]
  }
}

resource "kubernetes_cluster_role_binding" "fluentd" {
  metadata {
    name = "fluentd"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "ClusterRole"
    name = "${kubernetes_cluster_role.fluentd.metadata[0].name}"
  }
  subject {
    kind = "ServiceAccount"
    name = "${kubernetes_service_account.fluentd.metadata[0].name}"
    namespace = "kube-system"
  }
}

resource "kubernetes_config_map" "fluentd-config" {
  metadata {
    name = "fluentd-config"
    namespace = "kube-system"
    labels = {
      k8s-app = "fluentd-cloudwatch"
    }
  }
  data = {
    "fluent.conf" = <<EOF
@include containers.conf
@include systemd.conf

<match fluent.**>
@type null
</match>
EOF
    "containers.conf" = <<EOF
    <source>
      @type tail
      @id in_tail_container_logs
      @label @containers
      path /var/log/containers/*.log
      pos_file /var/log/fluentd-containers.log.pos
      tag *
      read_from_head true
      <parse>
        @type json
        time_format %Y-%m-%dT%H:%M:%S.%NZ
      </parse>
    </source>

    <label @containers>
      <filter **>
        @type kubernetes_metadata
        @id filter_kube_metadata
      </filter>

      <filter **>
        @type record_transformer
        @id filter_containers_stream_transformer
        <record>
          stream_name $${tag_parts[3]}
        </record>
      </filter>

      <match **>
        @type cloudwatch_logs
        @id out_cloudwatch_logs_containers
        region "#{ENV.fetch('REGION')}"
        log_group_name "/eks/#{ENV.fetch('CLUSTER_NAME')}/containers"
        log_stream_name_key stream_name
        remove_log_stream_name_key true
        auto_create_stream true
        <buffer>
          flush_interval 5
          chunk_limit_size 2m
          queued_chunks_limit_size 32
          retry_forever true
        </buffer>
      </match>
    </label>
EOF

    "systemd.conf" = <<EOF
    <source>
      @type systemd
      @id in_systemd_kubelet
      @label @systemd
      filters [{ "_SYSTEMD_UNIT": "kubelet.service" }]
      <entry>
        field_map {"MESSAGE": "message", "_HOSTNAME": "hostname", "_SYSTEMD_UNIT": "systemd_unit"}
        field_map_strict true
      </entry>
      path /run/log/journal
      pos_file /var/log/fluentd-journald-kubelet.pos
      read_from_head true
      tag kubelet.service
    </source>

    <source>
      @type systemd
      @id in_systemd_kubeproxy
      @label @systemd
      filters [{ "_SYSTEMD_UNIT": "kubeproxy.service" }]
      <entry>
        field_map {"MESSAGE": "message", "_HOSTNAME": "hostname", "_SYSTEMD_UNIT": "systemd_unit"}
        field_map_strict true
      </entry>
      path /run/log/journal
      pos_file /var/log/fluentd-journald-kubeproxy.pos
      read_from_head true
      tag kubeproxy.service
    </source>

    <source>
      @type systemd
      @id in_systemd_docker
      @label @systemd
      filters [{ "_SYSTEMD_UNIT": "docker.service" }]
      <entry>
        field_map {"MESSAGE": "message", "_HOSTNAME": "hostname", "_SYSTEMD_UNIT": "systemd_unit"}
        field_map_strict true
      </entry>
      path /run/log/journal
      pos_file /var/log/fluentd-journald-docker.pos
      read_from_head true
      tag docker.service
    </source>

    <label @systemd>
      <filter **>
        @type record_transformer
        @id filter_systemd_stream_transformer
        <record>
          stream_name $${tag}-$${record["hostname"]}
        </record>
      </filter>

      <match **>
        @type cloudwatch_logs
        @id out_cloudwatch_logs_systemd
        region "#{ENV.fetch('REGION')}"
        log_group_name "/eks/#{ENV.fetch('CLUSTER_NAME')}/systemd"
        log_stream_name_key stream_name
        auto_create_stream true
        remove_log_stream_name_key true
        <buffer>
          flush_interval 5
          chunk_limit_size 2m
          queued_chunks_limit_size 32
          retry_forever true
        </buffer>
      </match>
    </label>
EOF

  }
}

resource "kubernetes_daemonset" "fluentd-cloudwatch" {
  metadata {
    name = "fluentd-cloudwatch"
    namespace = "kube-system"
    labels = {
      k8s-app = "fluentd-cloudwatch"
    }
  }
  spec {
    selector {
          match_labels = {
            k8s-app = "fluentd-cloudwatch"
          }
        }
    template {
      metadata {
        labels = {
          k8s-app = "fluentd-cloudwatch"
        }
      }
      spec {
        service_account_name = "${kubernetes_service_account.fluentd.metadata[0].name}"
        termination_grace_period_seconds = 30
        automount_service_account_token = true
        init_container{
          name = "copy-fluentd-config"
          image = "busybox"
          command = ["sh", "-c", "cp /config-volume/..data/* /fluentd/etc"]
          volume_mount{
              name = "config-volume"
              mount_path = "/config-volume"
          }
          volume_mount{
              name = "fluentdconf"
              mount_path = "/fluentd/etc"
          }

        }
        container{
          name = "fluentd-cloudwatch"
          image = "fluent/fluentd-kubernetes-daemonset:v1.1-debian-cloudwatch"

          env{
              name = "REGION"
              value = "${var.current_aws_region}"
          }
          env{
              name = "CLUSTER_NAME"
              value = "${var.k8s-cluster-name}"
          }

          resources {
            limits {
              memory = "200Mi"
            }
            requests {
              cpu = "100m"
              memory = "200Mi"
            }
          }

          volume_mount{
            name = "config-volume"
            mount_path = "/config-volume"
          }
          volume_mount{
            name = "fluentdconf"
            mount_path = "/fluentd/etc"
          }
          volume_mount{
            name = "varlog"
            mount_path = "/var/log"
          }
          volume_mount{
            name = "varlibdockercontainers"
            mount_path = "/var/lib/docker/containers"
            read_only = true
          }
          volume_mount{
            name = "runlogjournal"
            mount_path = "/run/log/journal"
            read_only = true
          }
        }

        volume{
          name = "config-volume"
          config_map{
            name = "fluentd-config"
          }
        }
        volume{
          name = "fluentdconf"
          empty_dir{}
        }
        volume{
          name = "varlog"
          host_path{
            path = "/var/log"
          }
        }
        volume{
          name = "varlibdockercontainers"
          host_path{
            path = "/var/lib/docker/containers"
          }
        }
        volume{
          name = "runlogjournal"
          host_path{
            path = "/run/log/journal"
          }
        }
      }
    }
  }
}

