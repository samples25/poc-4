
resource "kubernetes_config_map" "aws_auth" {
  metadata {
    name = "aws-auth"
    namespace = "kube-system"
  }
  data = {
    mapRoles = "${var.config_map_maproles}"
    mapUsers = "${local.config_map_mapusers}"
  }
}