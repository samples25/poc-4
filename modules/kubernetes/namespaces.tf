resource "kubernetes_namespace" "prometheus" {
  metadata {
    name = "prometheus"
  }
}
resource "kubernetes_namespace" "grafana" {
  metadata {
    name = "grafana"
  }
}

resource "kubernetes_namespace" "poc4" {
  metadata {
    name = "poc4"
  }
}