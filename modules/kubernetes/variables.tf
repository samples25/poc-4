/*variable "var_label" {
  description = ""
}*/

variable "k8s-cluster-name" {

}

variable "current_aws_region" {

}

variable "config_map_maproles" {}


variable "kubeconfig" {}

variable "es_endpoint" {}

variable "kafka_endpoint" {}

variable "zookeeper_nodes" {}