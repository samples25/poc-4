resource "kubernetes_service" "es-endpoint" {
  metadata {
    name = "es"
    namespace = "poc4"
  }
  spec {
    type = "ExternalName"
    externalName = "${var.es_endpoint}"
  }
}

locals{
  kafka-endpoint-list = "${split(",", var.kafka_endpoint)}"
}

resource "kubernetes_service" "poc4-kafka-bootstrap" {
  count = 3
  metadata {
    name = "poc4-kafka-bootstrap"
    namespace = "poc4"
  }
  spec {
    type = "ExternalName"
    externalName = "${split(":",local.kafka-endpoint-list[count.index])}[0]"
    port {
      port = "${split(":",local.kafka-endpoint-list[count.index])}[1]"
    }
  }
}





