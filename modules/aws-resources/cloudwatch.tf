# Log Groups
resource "aws_cloudwatch_log_group" "log_group_containers" {
  name              = "poc4_log_group_containers"
  tags              = {
    env = "poc4"
  }
}

resource "random_string" "suffix" {
  length  = 4
  special = false
}

resource "aws_iam_policy" "policy_cloudwatch" {
  name        = "EKSCloudWatchLogPolicy-${random_string.suffix.result}"
  description = "This policy allow EKS workers to send logs into cloudwatch"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:CreateLogGroup",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams",
        "logs:PutLogEvents"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "cloudwatch-role-attach" {
  role = "${aws_iam_role.demo-node.name}"
  policy_arn = aws_iam_policy.policy_cloudwatch.arn
}

//

resource "aws_iam_role" "lambda_basic_execution" {
  name = "LambdaBasicExec"
  assume_role_policy = <<EOF
{
   "Version": "2012-10-17",
   "Statement": [
   {
     "Effect": "Allow",
     "Principal": {
        "Service": "lambda.amazonaws.com"
     },
   "Action": "sts:AssumeRole"
   }
 ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_basic_execution" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  role = "${aws_iam_role.lambda_basic_execution.name}"
}

/*
resource "aws_lambda_function" "cwl_stream_lambda" {
  function_name = "poc4_cwl_stream_lambda"
  handler = ""
  role = ""
  runtime = ""
}

resource "aws_lambda_permission" "cwl_lambda" {
  action = ""
  function_name = "${aws_lambda_function.cwl_stream_lambda.function_name}"
  principal = ""
}


resource "aws_cloudwatch_log_subscription_filter" "cloudwatch_logs_to_es" {
  name = "poc4_cloudwatch_logs_to_es"
  role_arn = "${aws_iam_role.lambda_basic_execution.arn}"
  log_group_name = "${aws_cloudwatch_log_group.log_group_containers.name}"
  filter_pattern = ""
  destination_arn = "${aws_elasticsearch_domain.poc4.}"
}
*/