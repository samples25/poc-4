//Docker repo
output "ecr_url" {
  value = "${aws_ecr_repository.poc4.repository_url}"
}

//Kafka

output "zookeeper_connect_string" {
  value = "${aws_msk_cluster.poc4.zookeeper_connect_string}"
}

output "bootstrap_brokers" {
  description = "Plaintext connection host:port pairs"
  value       = "${aws_msk_cluster.poc4.bootstrap_brokers}"

}

output "bootstrap_brokers_tls" {
  description = "TLS connection host:port pairs"
  value = "${aws_msk_cluster.poc4.bootstrap_brokers_tls}"
}


// K8s

output "endpoint" {
  value = "${aws_eks_cluster.poc4.endpoint}"
}

output "kubeconfig-certificate-authority-data" {
  value = "${aws_eks_cluster.poc4.certificate_authority.0.data}"
  sensitive   = true
}


//kubeconfig for kubectl
output "kubeconfig" {
  value = "${local.kubeconfig}"
  depends_on = [
    "aws_eks_cluster.poc4"
  ]
}


//eks configmap output
output "config_map_aws_auth" {
  value = "${local.config_map_aws_auth}"
  depends_on = [
    "aws_eks_cluster.poc4"
  ]
}

output "config_map_maproles" {
  value = "${local.config_map_maproles}"
  depends_on = [
    "aws_eks_cluster.poc4"
  ]
}

//es
output "kibana_endpoint" {
  value = "${aws_elasticsearch_domain.poc4.kibana_endpoint}"
}

output "es_endpoint" {
  value = "${aws_elasticsearch_domain.poc4.endpoint}"
}

/*
// kibana lb
output "kibana_lb_dns" {
  value = "${aws_lb.kibana-lb.dns_name}"
}*/
