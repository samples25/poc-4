resource "aws_cognito_user_pool" "poc4-pool" {
  name = "poc4-pool"
  tags = {
    env = "poc4"
  }
  admin_create_user_config {
    allow_admin_create_user_only = true
  }
}

resource "aws_cognito_user_pool_domain" "poc4-domain" {
  domain       = "poc4-domain"
  user_pool_id = "${aws_cognito_user_pool.poc4-pool.id}"
}

resource "aws_cognito_identity_pool" "poc4" {
  identity_pool_name = "poc4 identity pool"
  allow_unauthenticated_identities = false
}

/*resource "aws_iam_role" "authenticated" {
  name = "cognito_authenticated"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "cognito-identity.amazonaws.com"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "cognito-identity.amazonaws.com:aud": "${aws_cognito_identity_pool.poc4.id}"
        },
        "ForAnyValue:StringLike": {
          "cognito-identity.amazonaws.com:amr": "authenticated"
        }
      }
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "authenticated" {
  name = "authenticated_policy"
  role = "${aws_iam_role.authenticated.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "mobileanalytics:PutEvents",
        "cognito-sync:*",
        "cognito-identity:*"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}*/
/*

resource "aws_cognito_identity_provider" "poc4_provider" {
  user_pool_id  = "${aws_cognito_user_pool.poc4-pool.id}"
  provider_name = "LoginWithAmazon"
  provider_type = "LoginWithAmazon"
  //provider_details = {
    authorize_scopes = "profile"
    client_id = "${aws_cognito_user_pool_client.poc4-es-client.id}"
    client_secret = "${aws_cognito_user_pool_client.poc4-es-client.client_secret}"
  }
  attribute_mapping = {
    username = "user_id"
  }
}
*/


/*
resource "aws_cognito_identity_pool_roles_attachment" "main" {
  identity_pool_id = "${aws_cognito_identity_pool.poc4.id}"

  role_mapping {
    identity_provider         = "${aws_cognito_identity_provider.poc4_provider.provider_name}"
    ambiguous_role_resolution = "AuthenticatedRole"
    type                      = "Rules"

    mapping_rule {
      claim      = "isAdmin"
      match_type = "Equals"
      role_arn   = "${aws_iam_role.authenticated.arn}"
      value      = "paid"
    }
  }

  roles = {
    "authenticated" = "${aws_iam_role.authenticated.arn}"
  }
}
*/

/*resource "aws_cognito_user_pool_client" "poc4-es-client" {
  name = "poc4-es-client"
  user_pool_id = "${aws_cognito_user_pool.poc4-pool.id}"
  //callback_urls = "${aws_elasticsearch_domain.poc4.kibana_endpoint}"
  //logout_urls   = "${aws_elasticsearch_domain.poc4.kibana_endpoint}"
  generate_secret     = true
  allowed_oauth_scopes = ["profile"]
}*/
/*

//role with AmazonESCognitoAccess
resource "aws_iam_role" "es_cognito" {
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "es.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "es_cognito" {
  name = "es_cognito"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "iam:GetRole",
        "iam:PassRole",
        "iam:CreateRole",
        "iam:AttachRolePolicy",
        "ec2:DescribeVpcs",
        "cognito-identity:ListIdentityPools",
        "cognito-idp:ListUserPools"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "es_cognito" {
  policy_arn = "${aws_iam_policy.es_cognito.arn}"
  role = "${aws_iam_role.es_cognito.name}"
}
*/
