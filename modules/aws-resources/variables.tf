/*variable "var_label" {
  description = ""
}*/

//Kubernetes
variable "k8s-cluster-name" {
  default = "terraform-eks-poc4"
  type    = "string"
}

//
variable "ingress-workstation" {
  description = "local workstation external IP"
  type        = list
  default =  []
}

