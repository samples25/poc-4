
resource "aws_security_group" "sg" {
  vpc_id = "${aws_vpc.vpc.id}"
  ingress {
    from_port = 0
    protocol = "tcp"
    to_port = 65535
    cidr_blocks = ["192.168.0.0/16","172.31.16.0/20"]
  }
  egress {
    from_port = 0
    protocol = "tcp"
    to_port = 65535
    cidr_blocks = ["192.168.0.0/16","172.31.16.0/20"]
  }
}

resource "aws_msk_cluster" "poc4" {
  cluster_name           = "poc4"
  kafka_version          = "2.1.0"
  number_of_broker_nodes = 3

  broker_node_group_info {
    instance_type  = "kafka.m5.large"
    ebs_volume_size = "5"
    client_subnets = "${aws_subnet.subnets.*.id}"
    security_groups = [ "${aws_security_group.sg.id}" ]
  }

  encryption_info {
    encryption_at_rest_kms_key_arn = "${aws_kms_key.poc4-kms.arn}"
    encryption_in_transit {
      client_broker = "PLAINTEXT"
    }
  }

  tags = {
    env = "poc4"
  }

  lifecycle {
    ignore_changes = [ encryption_info,]
  }
}

