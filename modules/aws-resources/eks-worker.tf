// role and instance profile
resource "aws_iam_role" "demo-node" {
  name = "terraform-eks-demo-node"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY

}

resource "aws_iam_role_policy_attachment" "demo-node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role = "${aws_iam_role.demo-node.name}"
}

resource "aws_iam_role_policy_attachment" "demo-node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role = "${aws_iam_role.demo-node.name}"
}

resource "aws_iam_role_policy_attachment" "demo-node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role = "${aws_iam_role.demo-node.name}"
}

resource "aws_iam_instance_profile" "demo-node" {
  name = "terraform-demo-eks-node"
  role = "${aws_iam_role.demo-node.name}"
}

//permissions for nodes to send logs and create log groups and log streams

resource "aws_iam_policy" "logs-policy" {
  name = "logs-policy"
  policy = <<EoF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "logs:DescribeLogGroups",
                "logs:DescribeLogStreams",
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "*",
            "Effect": "Allow"
        }
    ]
}
EoF

}

resource "aws_iam_role_policy_attachment" "demo-node-logs-policy" {
  policy_arn = "${aws_iam_policy.logs-policy.arn}"
  role = "${aws_iam_role.demo-node.name}"
}

// security group
resource "aws_security_group" "demo-node" {
  name = "terraform-eks-demo-node"
  description = "Security group for all nodes in the cluster"
  vpc_id = "${aws_vpc.vpc.id}"

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  tags = "${
    map(
     "Name", "terraform-eks-demo-node",
     "kubernetes.io/cluster/${var.k8s-cluster-name}", "owned",
    )
  }"
}

resource "aws_security_group_rule" "demo-node-ingress-self" {
  description = "Allow node to communicate with each other"
  from_port = 0
  protocol = "-1"
  security_group_id = "${aws_security_group.demo-node.id}"
  source_security_group_id = "${aws_security_group.demo-node.id}"
  to_port = 65535
  type = "ingress"
}

resource "aws_security_group_rule" "demo-node-ingress-cluster" {
  description = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port = 1025
  protocol = "tcp"
  security_group_id = "${aws_security_group.demo-node.id}"
  source_security_group_id = "${aws_security_group.demo-cluster.id}"
  to_port = 65535
  type = "ingress"
}

// worker access to eks cluster
resource "aws_security_group_rule" "demo-cluster-ingress-node-https" {
  description = "Allow pods to communicate with the cluster API Server"
  from_port = 443
  protocol = "tcp"
  security_group_id = "${aws_security_group.demo-cluster.id}"
  source_security_group_id = "${aws_security_group.demo-node.id}"
  to_port = 443
  type = "ingress"
}

//autoscaling
data "aws_ami" "eks-worker" {
  filter {
    name = "name"
    values = [
      "amazon-eks-node-${aws_eks_cluster.poc4.version}-v*"]
  }
  owners = [
    "amazon"]
  most_recent = true
}


locals {
  demo-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.poc4.endpoint}' --b64-cluster-ca '${aws_eks_cluster.poc4.certificate_authority.0.data}' '${var.k8s-cluster-name}'
USERDATA
}

resource "aws_launch_configuration" "demo" {
  associate_public_ip_address = true
  iam_instance_profile = "${aws_iam_instance_profile.demo-node.name}"
  image_id = "${data.aws_ami.eks-worker.id}"
  instance_type = "m5d.2xlarge"
  name_prefix = "terraform-eks-demo"
  security_groups = [
    "${aws_security_group.demo-node.id}"]
  user_data_base64 = "${base64encode(local.demo-node-userdata)}"
  //user_data = "${local.demo-node-userdata}"
  key_name = "mykey"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "demo" {
  desired_capacity = 2
  launch_configuration = "${aws_launch_configuration.demo.id}"
  max_size = 2
  min_size = 1
  name = "terraform-eks-demo"
  vpc_zone_identifier = flatten( ["${aws_subnet.eks-subnets.*.id}"])
  tag {
    key = "Name"
    value = "terraform-eks-demo"
    propagate_at_launch = true
  }
  tag {
    key = "kubernetes.io/cluster/${var.k8s-cluster-name}"
    value = "owned"
    propagate_at_launch = true
  }
}

// allow to push to es

resource "aws_iam_policy" "policy_es" {
  name        = "EKSESPolicy"
  description = "This policy allow EKS workers to send logs into es"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "es:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "es-role-attach" {
  role = "${aws_iam_role.demo-node.name}"
  policy_arn = aws_iam_policy.policy_es.arn
}



