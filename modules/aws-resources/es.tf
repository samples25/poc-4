data "aws_instances" "workers" {
        instance_tags = {
          aws:autoscaling:groupName = "${aws_autoscaling_group.demo.name}"
        }
}

locals {
  count    = "${length(data.aws_instances.workers.ids)}"
  ips = "${data.aws_instances.workers.public_ips[count.index]}"
}

variable "domain" {
  default = "tf-poc4"
}

data "aws_caller_identity" "current" {}


resource "aws_iam_service_linked_role" "elasticsearch-poc4" {
  aws_service_name = "es.amazonaws.com"
}

/*
resource "aws_security_group" "es" {
  name        = "elasticsearch-${var.domain}"
  description = "Managed by Terraform"
  vpc_id      = "${aws_vpc.vpc.id}"

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = [
      "${aws_vpc.vpc.cidr_block}",
    ]
  }
}
*/


resource "aws_elasticsearch_domain" "poc4" {

  domain_name           = "${var.domain}"
  elasticsearch_version = "6.3"

  cluster_config {
    instance_type = "r4.large.elasticsearch"
    instance_count = 1
  }

  ebs_options {
    ebs_enabled = true
    volume_size = 10
  }

 /* cognito_options {
    enabled = true
    identity_pool_id = "${aws_cognito_identity_pool.poc4.id}"
    role_arn = "${aws_iam_role.es_cognito.arn}"
    user_pool_id = "${aws_cognito_user_pool.poc4-pool.id}"
  }*/

  access_policies = <<CONFIG
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "es:*",
      "Resource": "arn:aws:es:ap-southeast-1:321366902288:domain/tf-poc4/*",
      "Condition": {
        "IpAddress": {
          "aws:SourceIp": [
            "41.87.135.146",
            "195.200.164.180",
            ${local.ips_list}
          ]
        }
      }
    }
  ]
}
CONFIG
}

/*

resource "aws_elasticsearch_domain" "poc4" {

  domain_name           = "${var.domain}"
  elasticsearch_version = "6.3"

  */
/*  cognito_options {
    enabled = true
    identity_pool_id = ""
    role_arn = ""
    user_pool_id = ""
  }*//*


  cluster_config {
    instance_type = "r4.large.elasticsearch"
    instance_count = 1
  }

  ebs_options {
    ebs_enabled = true
    volume_size = 10
  }

  vpc_options {
    security_group_ids = ["${aws_security_group.es.id}"]
    subnet_ids =  flatten(["${aws_subnet.es-subnets.*.id}"])
  }

}

*/

resource "aws_elasticsearch_domain_policy" "main" {
  domain_name = "${aws_elasticsearch_domain.poc4.domain_name}"

  access_policies = <<POLICIES
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Principal": {
               "AWS": "*"
             },
            "Effect": "Allow",
            "Resource": "${aws_elasticsearch_domain.poc4.arn}"
        }
    ]
}
POLICIES
}

/*
resource "aws_iam_service_linked_role" "es" {
  aws_service_name = "es.amazonaws.com"
}
*/

