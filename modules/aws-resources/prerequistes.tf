data "aws_region" "current" {}

data "aws_availability_zones" "azs" {
  state = "available"
}

resource "aws_vpc" "vpc" {
  cidr_block = "192.168.0.0/16"
  enable_dns_hostnames = true
  tags = {
    NAME = "poc4-vpc"
    PROJECT = "poc4"
  }
}

//general use private subnets
resource "aws_subnet" "subnets" {
  count = 3
  availability_zone = "${data.aws_availability_zones.azs.names[count.index]}"
  cidr_block        = "192.168.${count.index}.0/24"
  vpc_id            = "${aws_vpc.vpc.id}"
}

//subnets to be used by eks
resource "aws_subnet" "eks-subnets" {
  count = 2

  availability_zone = "${data.aws_availability_zones.azs.names[count.index]}"
  cidr_block        = "192.168.${count.index + 3}.0/24"
  vpc_id            = "${aws_vpc.vpc.id}"

  tags = "${
    map(
     "Name", "terraform-eks",
     "kubernetes.io/cluster/${var.k8s-cluster-name}", "shared",
    )
  }"
}

// subnet to be used by es
/*
resource "aws_subnet" "es-subnets" {
  count = 1
  availability_zone = "${data.aws_availability_zones.azs.names[count.index]}"
  cidr_block        = "192.168.${count.index + 5}.0/24"
  vpc_id            = "${aws_vpc.vpc.id}"
}
*/

resource "aws_kms_key" "poc4-kms" {
  description = "poc4 kms"
}

resource "aws_internet_gateway" "poc4" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name = "terraform-poc4"
  }
}

resource "aws_route_table" "poc4" {
  vpc_id = "${aws_vpc.vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.poc4.id}"
  }
  route {
    cidr_block = "${data.aws_vpc.default.cidr_block}"
    vpc_peering_connection_id = "${aws_vpc_peering_connection.poc4-to-default.id}"
  }
}

data "aws_subnet_ids" "poc4_subnets" {
  vpc_id = "${aws_vpc.vpc.id}"
  depends_on = ["aws_subnet.eks-subnets","aws_subnet.subnets"]
}

resource "aws_route_table_association" "poc4" {
  count = 5
  subnet_id = "${sort(data.aws_subnet_ids.poc4_subnets.ids)[count.index]}"
  route_table_id = "${aws_route_table.poc4.id}"
  depends_on = ["aws_subnet.eks-subnets","aws_subnet.subnets"]
}

data "aws_vpc" "default"{
  default = true
}

data "aws_instance" "launch-cron" {
  filter {
    name = "tag:Name"
    values = ["CronMachine"]
  }
  filter {
    name = "tag:Project"
    values = ["POC4"]
  }
}

resource "aws_vpc_peering_connection" "poc4-to-default" {
  peer_vpc_id = "${data.aws_vpc.default.id}"
  vpc_id = "${aws_vpc.vpc.id}"
  auto_accept = true
  accepter {
    allow_remote_vpc_dns_resolution = true
  }

  requester {
    allow_remote_vpc_dns_resolution = true
  }
}

data "aws_route_table" "default_main" {
  vpc_id = "${data.aws_vpc.default.id}"
}

resource "aws_route" "default_to_poc4" {
  route_table_id = "${data.aws_route_table.default_main.route_table_id}"
  destination_cidr_block = "${aws_vpc.vpc.cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.poc4-to-default.id}"
}