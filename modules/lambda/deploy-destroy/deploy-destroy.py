# -*- coding: utf-8 -*-

# inspired from https://github.com/FitnessKeeper/terraform-lambda/blob/master/service.py

import os
import subprocess
import urllib
import boto3


TERRAFORM_VERSION = '0.12.3'

TERRAFORM_DOWNLOAD_URL = (
    'https://releases.hashicorp.com/terraform/%s/terraform_%s_linux_amd64.zip'
    % (TERRAFORM_VERSION, TERRAFORM_VERSION))

# Paths where Terraform should be installed
TERRAFORM_DIR = os.path.join('/tmp', 'terraform_%s' % TERRAFORM_VERSION)
TERRAFORM_PATH = os.path.join(TERRAFORM_DIR, 'terraform')

def check_call(args):
    """Wrapper for subprocess that checks if a process runs correctly,
    and if not, prints stdout and stderr.
    """
    proc = subprocess.Popen(args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        cwd='/tmp')
    stdout, stderr = proc.communicate()
    if proc.returncode != 0:
        print(stdout)
        print(stderr)
        raise subprocess.CalledProcessError(
            returncode=proc.returncode,
            cmd=args)


def install_terraform():
    """Install Terraform on the Lambda instance."""
    # Most of a Lambda's disk is read-only, but some transient storage is
    # provided in /tmp, so we install Terraform here.  This storage may
    # persist between invocations, so we skip downloading a new version if
    # it already exists.
    # http://docs.aws.amazon.com/lambda/latest/dg/lambda-introduction.html
    if os.path.exists(TERRAFORM_PATH):
        return

    urllib.urlretrieve(TERRAFORM_DOWNLOAD_URL, '/tmp/terraform.zip')

    # Flags:
    #   '-o' = overwrite existing files without prompting
    #   '-d' = output directory
    check_call(['unzip', '-o', '/tmp/terraform.zip', '-d', TERRAFORM_DIR])

    check_call([TERRAFORM_PATH, '--version'])

def get_plan(git_repo,path):
    """
    :param git_repo:
    :param path
    :return:
    """
    client = boto3.client('codecommit',region_name = "ap-southeast-1")
    response = client.get_folder(
        repositoryName='POC4-IaC',
        commitSpecifier='wip',
        folderPath="modules/aws-resources"
    )


def apply_terraform_plan(path):
    """run a 'terraform apply'.
    :param path: Path to the Terraform planfile in repo.
    """
    os.chdir("/tmp/path")
    check_call([TERRAFORM_PATH, 'apply'])


def handler(event, context):
    git_repo=""
    path= "modules/aws-resources"
    install_terraform()
    get_plan(git_repo=git_repo, path=path)
    apply_terraform_plan(path=path)
