//Allows Lambda functions to call AWS services on your behalf

data "aws_iam_policy_document" "lambda-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "lambda_execution_role" {
  name = "lambda_execution_role"

  assume_role_policy = "${data.aws_iam_policy_document.lambda-assume-role-policy.json}"

  tags = {
    Description = "Allows Lambda functions to call AWS services on your behalf."
  }
}

