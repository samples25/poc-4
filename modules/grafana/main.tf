resource "grafana_organization" "poc4" {
  name = "poc4"
  create_users = true
  viewers = [
    "poc4-viewer@yourdomain.com"
  ]
}

resource "grafana_dashboard" "Kubernetes_All_Nodes" {
  config_json = "${file("${path.module}/dashboards/Kubernetes_All_Nodes-1563443258812.json")}"
}

resource "grafana_dashboard" "Kubernetes_Pods" {
  config_json = "${file("${path.module}/dashboards/Kubernetes_Pods-1563443629947.json")}"
}
